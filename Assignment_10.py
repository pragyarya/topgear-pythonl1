number = 1000000000
#number = -1000000000
ls = range(1000000)
count = 0
target = 0

for i in range(len(ls)):
    target = target + ls[i]

    if target > number:
        target = target - ls[i] * 2

    if target == number:
        break
    count = count + 1

print("Target-->", target)
print("Count-->", count)
