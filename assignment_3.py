# data = ['bbb', 'ccc', 'axx', 'xzz', 'xaa']
data= ['mix', 'xyz', 'apple', 'xanadu', 'aardvark']

x_data = []
rest_data = []

for i in data:
    if i[0] == 'x':
        x_data.append(i)
    else:
        rest_data.append(i)

print(sorted(x_data) + sorted(rest_data))
